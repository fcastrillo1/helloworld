# Hello World webapp

Hello World web application simplest project

## Requisitos

* [Maven](http://maven.apache.org/download.cgi)
* Java  

## Build instructions
`mvn clean package`

## Create a Docker image with Liberty container
`docker build -t helloworld-webapp .`

## Run the Docker image
`docker run -p 8080:9080 helloworld-webapp`

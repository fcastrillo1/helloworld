FROM websphere-liberty:latest

RUN chown -R 1001:0 /config/dropins/
COPY target/helloworld-1.0-SNAPSHOT.war /config/dropins/
